import React from 'react';
import styles from './ImageInput.module.css';

export default class ImageInput extends React.Component {
	constructor(props) {
		super(props);
		this.setImg = this.setImg.bind(this);
		this.state = ({img: null});
	}

	setImg(e) {
		let {setWP} = this.context;
		let f = e.target.files[0];
		let u = URL.createObjectURL(f);
		let i = new Image();
		i.src = u;
		i.onload = e => {
			URL.revokeObjectURL(f);
		}
		setWP(i);
	}

	render() {
		return(
			<label
				htmlFor={this.inputRef}
				className={styles.inputLabel}
			>
				{this.props.label}
			<input
				className={styles.fileInput}
				type='file'
				accepted='image/*'
				onInput={this.setImg}
			/>
			</label>
		);
	}
}
