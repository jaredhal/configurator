
import React from 'react';
import styles from './ColorInput.module.css';

export default class ColorInput extends React.Component {
	constructor(props) {
		super(props);
		this.state = {color: this.props.color};
		this.handleChange = this.handleChange.bind(this);
	}

	handleChange(e) {
		e.preventDefault();
		this.setState({color: e.target.value});
		this.props.colorChange(e.target.value);
	}

	render() {
		let hexClasses, colorClasses;
		if (this.props.className) {
			hexClasses = styles.hexInput + ' ' + this.props.className;
			colorClasses = styles.colorInput + ' ' + this.props.className;
		} else {
			hexClasses = styles.hexInput;
			colorClasses = styles.colorInput;
		}
		return (
			<div>
				<label htmlFor={this.props.id}>
					{this.props.id}
				</label>
				<input
					className={hexClasses}
					id={this.props.id}
					type='text'
					value={this.state.color}
					onChange={this.handleChange}
					maxLength={7}
					minLength={7}
					pattern="#[0-f]{6}"
				/>
				<input
					className={colorClasses}
					type='color'
					value={this.state.color}
					onChange={this.handleChange}
				/>
			</div>
		);
	}
}
