import React from 'react';
import ThemeContext, {ThemeProvider} from './ThemeContext';
import Main from './Main';

import './App.css';
import './Theme.css';


export default () => {
	return (
		<ThemeProvider>
			<Main />
		</ThemeProvider>
	);
}
