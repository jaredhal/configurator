import React from 'react';
import styles from './PreviewWindow.module.css';

export default (props) => {
	return(
		<div className={styles.wrapper}>
			<div className={styles.topBar}>{props.title}</div>
			<div className={styles.content}>{props.children}</div>
		</div>
	);
};
