import React from 'react';
import styles from './Frame.module.css';

export default class Frame extends React.Component {
	constructor(props) {
		super(props);
		this.state = {expanded: false};
		this.expandAnimation = React.createRef();
		this.collapseAnimation = React.createRef();
		this.toggle = this.toggle.bind(this);
	}

	toggle(e) {
		if (this.state.expanded) {
			this.collapseAnimation.current.beginElement();
		} else {
			this.expandAnimation.current.beginElement();
		}
		this.setState({ expanded: !this.state.expanded });
	}

	render() {
		return(
			<div className={styles.wrapper}>
				<div className={styles.header} onClick={ e => this.toggle(e) }>
					<h2>{this.props.header}</h2>
					<svg className={styles.svg} width="33" height="33" viewBox="0 0 33 33"
						xmlns="http://www.w3.org/2000/svg" width="33" height="33"
					>
						<polygon points="10,9 10,25 24,17">
							<animateTransform ref={this.expandAnimation}
								attributeType="XML"
								attributeName="transform"
								type="rotate"
								dur="0.25s"
								from="0 17,17"
								to="90 17,17"
								fill="freeze"
							/>
							<animateTransform ref={this.collapseAnimation}
								attributeType="XML"
								attributeName="transform"
								type="rotate"
								dur="0.25s"
								from="90 17,17"
								to="0 17,17"
								fill="freeze"
							/>
						</polygon>
					</svg>
				</div>
				{ this.state.expanded ? this.props.children : null }
			</div>
		);
	}
}
