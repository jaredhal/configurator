import React from 'react';

export default() => {
	return (
		<code>
			<pre>{`1 #!/bin/bash
2
3 set -eu
4 function contrast {
5    if [  $1 -lt 16 ]; then #OG 15, so either black or white
6       [ $1 -eq 0 ] && printf '15' || printf '0'
7       return
8    elif (( $1 > 231)); then #greyscale
9       (( $1 < 244)) && printf '15' || printf '0'
10       return
11    else #everything else
12       l=$(( ( ($1 - 16) % 36) / 6 ))
13       (( l > 2)) && printf '0' || printf '15'
14       return
15    fi
16 }
17
18 function print_color {
19    printf '\\e[48;5;%sm' '$1' #sets bg to '$1'
20    printf '\\e[38;5;%sm' '$( contrast $1 )' #sets fg to contrast output of $1
21    printf ' $2 '
22    printf '\\e[0m' #resets terminal
23 }
24
25 for (( c=0; c<16; c++ )) d
26    print_color $c $c
27 done
28 printf '\\n'`
			}</pre>
		</code>
	);
};
