import React from 'react';
import styles from './InputContainer.module.css';
import ColorInput from './ColorInput';
import ThemeContext from './ThemeContext';

export default class InputContainer extends React.Component {
	constructor(props) {
		super(props);
		this.state = {expanded: false};
		this.setThemeColor = this.setThemeColor.bind(this);
	}

	setThemeColor(id, colorVal) {
		let {setBG, setFG, setColor} = this.context;
		console.log('heres the context');
		if (id === 'bg') {
			setBG(colorVal);
		} else if (id === 'fg') {
			setFG(colorVal);
		} else if ( id.substring(0,5) === 'color'
			&& parseInt(id.substring(5,7)) <= 15
			&& parseInt(id.substring(5,7)) >= 0
		) {
			setColor(id, colorVal);
		} else {
			console.log('bad values for setThemeColor:', id, colorVal);
		}
	}

	render() {
		let { fg, bg, colors, setFG, setBG, setColor} = this.context;
		let colorInputs = Object.keys(colors).map( (name, i) => {
			return (<ColorInput
				key={i}
				id={name}
				label={name}
				className={"--" + name}
				color={colors[name]}
				colorChange={ (newColor) => {
					setColor(name, newColor);
				}}
			/>);
		});
		let colorInputsOne = colorInputs.slice(0,8);
		let colorInputsTwo = colorInputs.slice(8,16);
		return (
			<div className={styles.rowsContainer}>
				<div className={styles.row}>
					<ColorInput
						key={-1}
						id={'bg'}
						label={'background'}
						className={"--bg"}
						color={bg}
						colorChange={ newColor => setBG(newColor)}
					/>
					<ColorInput
						key={-2}
						id={'fg'}
						label={'foreground'}
						className={"--fg"}
						color={fg}
						colorChange={ newColor => setFG(newColor)}
					/>
				</div>
				<div className={styles.colsContainer}>
					<div className={styles.column}>{colorInputsOne}</div>
					<div className={styles.column}>{colorInputsTwo}</div>
				</div>
			</div>
		);
	}
}
InputContainer.contextType = ThemeContext;
