import React from 'react';

const defaultColors = {
	color0: '#000000'
	,color1: '#aa0000'
	,color2: '#00aa00'
	,color3: '#aa5500'
	,color4: '#0000aa'
	,color5: '#aa00aa'
	,color6: '#00aaaa'
	,color7: '#aaaaaa'
	,color8: '#ffffff'
	,color9: '#ffffff'
	,color10: '#ffffff'
	,color11: '#ffffff'
	,color12: '#ffffff'
	,color13: '#ffffff'
	,color14: '#ffffff'
	,color15: '#ffffff'
};

const defaultTheme = {
	bg: '#000000'
	,fg: '#ffffff'
	,wp: null
	,colors: defaultColors
};

const ThemeContext = React.createContext();
export default ThemeContext;

export class ThemeProvider extends React.Component {
	constructor(props) {
		super(props);
		this.state = defaultTheme;
		this.setBG = this.setBG.bind(this);
		this.setFG = this.setFG.bind(this);
		this.setWP = this.setWP.bind(this);
		this.setColor = this.setColor.bind(this);
	}

	componentDidMount() {
		let html = document.getElementsByTagName('html')[0];
		for (const clr in this.state.colors) {
			//connect theme colors to CSS variables
			html.style.setProperty("--" + clr, this.state.colors[clr]);
		}
		html.style.setProperty('--bg',this.state.bg);
		html.style.setProperty('--fg', this.state.fg);
	}

	setBG (clr) {
		let html = document.getElementsByTagName('html')[0];
		this.setState({bg: clr});
		html.style.setProperty('--bg', clr);
	}

	setFG (clr) {
		let html = document.getElementsByTagName('html')[0];
		this.setState({fg: clr});
		html.style.setProperty('--fg', clr);
	}

	setWP (newWP) {
		let wpVal = null;
		console.log('setting wallpaper to', newWP);
		if (newWP instanceof Image) {
			console.log('its an instance of image');
			wpVal = newWP;
		} else if (newWP === 'pattern') {
			console.log('doing patterns');
		} else {
			wpVal = null;
		}
		this.setState({wp: wpVal});
	};

	setColor (clr, val) {
		let html = document.getElementsByTagName('html')[0];
		let newColors = {...this.state.colors};
		newColors[clr] = val;
		console.log('colors was', this.state.colors,'and now its ', newColors);
		this.setState({colors: newColors});
		html.style.setProperty('--' + clr, val);
	};

	render(){
		return (
			<ThemeContext.Provider
				value={{
					bg: this.state.bg
					,fg: this.state.fg
					,wp: this.state.wp
					,colors: this.state.colors
					,setBG: this.setBG
					,setFG: this.setFG
					,setWP: this.setWP
					,setColor: this.setColor
				}}
			>
				{this.props.children}
			</ThemeContext.Provider>
		);
	}
};
