import React from 'react';
import Preview from './Preview';
import Frame from './Frame';
import ImageInput from './ImageInput';
import InputContainer from './InputContainer';

import ThemeContext from './ThemeContext';


Preview.contextType = ThemeContext;
ImageInput.contextType = ThemeContext;
InputContainer.contextType = ThemeContext;

export default () => {
	return (
		<div>
			<Preview width={600} height={400} />
			<Frame header={'Colors'}>
				<InputContainer />
			</Frame>
			<Frame header={'Wallpaper'}>
				<ImageInput label={'upload wallpaper'} />
			</Frame>
			<Frame header={'Settings'}>
				<span>settings go here</span>
			</Frame>
		</div>
	);
}
