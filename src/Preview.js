import React from 'react';
import lodash from 'lodash';

import ThemeContext from './ThemeContext';
import TerminalPreview from './TerminalPreview';
import VimPreview from './VimPreview';
import PreviewWindow from './PreviewWindow';

import styles from './Preview.module.css';


export default class Preview extends React.Component {

	render() {
		let { colors, fg, bg, wp } = this.context;
		let divStyle;
		if ( wp instanceof Image ) {
			divStyle = { backgroundImage: 'url(' + wp.src +')'};
		} else if (wp === null) {
			divStyle = { backgroundColor: bg };
		}
		lodash.merge(divStyle, {
			width: this.props.width + 'px'
			,height: this.props.height + 'px'
		});
		return (
			<div style={divStyle} className={styles.previewPane}>
				<div className={styles.windowContainer}>
					<div className={styles.column}>
						<PreviewWindow title={'terminal'}>
							<TerminalPreview />
						</PreviewWindow>
					</div>
					<div className={styles.column}>
						<PreviewWindow title={'editor'}>
							<VimPreview />
						</PreviewWindow>
					</div>
				</div>
			</div>
		);
	}
}
