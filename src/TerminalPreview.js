import React from 'react';
import styles from './TerminalPreview.module.css';

export default() => {
	return (
		<code>
			<p className={'textColorFG'}>{"user@hostname:~$ ls -a"} </p>
			<div className={styles.container}>
				<span className={'textColor6'}>audio_file.ogg</span>
				&nbsp;
				<span className={'textColor1'}><b>archive_file.tar.gz</b></span>
				&nbsp;
				<span className={'textColor4'}><b>directory</b></span>
				&nbsp;
				<span className={'textColor2'}><b>executable_file.sh</b></span>
				&nbsp;
				<span className={'textColorFG'}>normal_file.txt </span>
				&nbsp;
				<span className={'textColor5'}><b>image_file.png</b></span>
				&nbsp;
				<span className={'textColor6'}><b>sym_link</b></span>
				&nbsp;
				<span className={'textColor3'}>named_pipe </span>
				&nbsp;
				<span className={'textColor0 bgColor2'}>sticky_other_writable</span>
				&nbsp;
				<span className={'textColor7 bgColor4'}>sticky_not_other_writable</span>
			</div>
		</code>
	);
};
